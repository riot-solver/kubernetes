import os

import asyncpg
import fastapi
import uvicorn
from fastapi import FastAPI, UploadFile
from fastapi.responses import Response
from minio import Minio


POSTGRESQL_DSN = os.getenv("POSTGRESQL_DSN", 123)
MINIO_DSN = os.getenv("MINIO_DSN", "92.255.174.30:9000")
MINIO_ACCESS_KEY = os.getenv("MINIO_ACCESS_KEY", "2rPn9jXPikKGHoP6dC89")
MINIO_SECRET_KEY = os.getenv("MINIO_SECRET_KEY", "jrRHUrAfjByP144FKKddFUSPXLskg5Jpoz1rqY4c")
MINIO_SESSION_KEY = os.getenv("MINIO_SESSION_KEY", None)


if not POSTGRESQL_DSN:
    raise ValueError("Не передано значение для POSTGRESQL_DSN.")

if not MINIO_DSN:
    raise ValueError("Не передано значение для MINIO_DSN.")


application = FastAPI()


@application.get("/postgresql", summary="Проверить подключение к PostgreSQL.")
async def test_postgresql() -> dict:
    connection: asyncpg.Connection = await asyncpg.connect(POSTGRESQL_DSN)
    result = await connection.fetchrow("SELECT 1")

    return dict(result)


@application.post("/minio/{folder}/{title}", summary="Добавит файл.")
async def add_file(
        folder: str,
        file: UploadFile
) -> None:
    minio = Minio(
        MINIO_DSN, MINIO_ACCESS_KEY, MINIO_SECRET_KEY, MINIO_SESSION_KEY, cert_check=False, secure=False
    )

    if not minio.bucket_exists(folder):
        minio.make_bucket(folder)

    minio.put_object(folder, file.filename, file.file, file.size)


@application.get("/minio", summary="Получить список файлов.")
async def get_files() -> dict[str, list[str]]:
    minio = Minio(
        MINIO_DSN, MINIO_ACCESS_KEY, MINIO_SECRET_KEY, MINIO_SESSION_KEY, cert_check=False, secure=False
    )

    files = {}

    folders = minio.list_buckets()
    for folder in folders:
        files[folder.name] = [file.object_name for file in minio.list_objects(folder.name)]

    return files


@application.get("/minio/{folder}/{title}", summary="Получить файл.")
async def get_file(
        folder: str,
        title: str
) -> Response:
    minio = Minio(
        MINIO_DSN, MINIO_ACCESS_KEY, MINIO_SECRET_KEY, MINIO_SESSION_KEY, cert_check=False, secure=False
    )

    if not minio.bucket_exists(folder):
        raise fastapi.HTTPException(404, "Не найдена папка Minio.")

    file = minio.get_object(folder, title)

    headers = {
        "Content-Disposition": f"attachment; filename={title}"
    }

    # Возвращаем файл как объект Response с установленными заголовками
    return Response(content=file.read(), media_type="application/octet-stream", headers=headers)


@application.delete("/minio/{folder}/{title}", summary="Удалить файл.")
async def delete_file(
        folder: str,
        title: str
) -> None:
    minio = Minio(
        MINIO_DSN, MINIO_ACCESS_KEY, MINIO_SECRET_KEY, MINIO_SESSION_KEY, cert_check=False, secure=False
    )

    if not minio.bucket_exists(folder):
        raise fastapi.HTTPException(404, "Не найдена папка Minio.")

    minio.remove_object(folder, title)


if __name__ == '__main__':
    uvicorn.run(application, host="0.0.0.0", port=80)
