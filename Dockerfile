FROM python:3.12-slim

WORKDIR /app
EXPOSE 80

RUN apt-get update
RUN python -m pip install --upgrade pip

COPY ./requirements.txt /app/
RUN pip install -r requirements.txt

COPY . .
ENTRYPOINT ["sh", "./run.sh"]